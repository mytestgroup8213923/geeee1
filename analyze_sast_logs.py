import json
import os

def analyze_sast_report(report_path):
    with open(report_path, 'r') as file:
        report = json.load(file)

    vulnerabilities = report.get('vulnerabilities', [])

    critical_vulns = [v for v in vulnerabilities if v['severity'] == 'Critical']
    high_vulns = [v for v in vulnerabilities if v['severity'] == 'High']

    return critical_vulns, high_vulns

def main():
    report_path = os.getenv('SAST_REPORT_PATH', 'gl-sast-report.json')
    output_file_path = 'sast_analysis_output.txt'

    if not os.path.exists(report_path):
        print(f"SAST report not found: {report_path}")
        return

    critical_vulns, high_vulns = analyze_sast_report(report_path)

    with open(output_file_path, 'w') as output_file:
        output_file.write(f"Critical Vulnerabilities: {len(critical_vulns)}\n")
        for vuln in critical_vulns:
            output_file.write(f" - {vuln['name']}: {vuln['description']}\n")

        output_file.write(f"\nHigh Vulnerabilities: {len(high_vulns)}\n")
        for vuln in high_vulns:
            output_file.write(f" - {vuln['name']}: {vuln['description']}\n")

    print(f"SAST analysis completed. Results saved to {output_file_path}")

if __name__ == "__main__":
    main()
